# CS 373: Software Engineering Collatz Repo

* Name: Jaden Hyde

* EID: jah8624

* GitLab ID: jaden20

* HackerRank ID: jadenhyde201

* Git SHA: 9774442fc1f40c8593d1d23acf984dc2b6969b9f

* GitLab Pipelines: https://gitlab.com/jaden20/cs373-collatz/-/pipelines

* Estimated completion time: 10

* Actual completion time: 12

* Comments: N/A
