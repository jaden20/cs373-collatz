#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import (
    collatz_read,
    collatz_eval,
    collatz_print,
    collatz_solve,
    cycle_length,
    build_cache,
)

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        build_cache()
        self.assertEqual(collatz_read("1 10\n"), (1, 10))

    # ----
    # eval
    # ----

    def test_eval_1(self):
        build_cache()
        self.assertEqual(collatz_eval((1, 10)), (1, 10, 20))

    def test_eval_2(self):
        build_cache()
        self.assertEqual(collatz_eval((100, 200)), (100, 200, 125))

    def test_eval_3(self):
        build_cache()
        self.assertEqual(collatz_eval((201, 210)), (201, 210, 89))

    def test_eval_4(self):
        build_cache()
        self.assertEqual(collatz_eval((900, 1000)), (900, 1000, 174))

    # -----
    # print
    # -----

    def test_print(self):
        sout = StringIO()
        collatz_print(sout, (1, 10, 20))
        self.assertEqual(sout.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        sin = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        sout = StringIO()
        collatz_solve(sin, sout)
        self.assertEqual(
            sout.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )

    # ------------
    # cycle_length
    # ------------

    def test_cycle_length(self):
        num = 5
        result = cycle_length(num)
        self.assertEqual(result, 6)

    def test_cycle_length_2(self):
        build_cache()
        self.assertEqual(cycle_length(997823), 440)

    def test_cycle_length_3(self):
        build_cache()
        self.assertEqual(cycle_length(680303), 248)

    def test_cycle_length_4(self):
        build_cache()
        self.assertEqual(cycle_length(733945), 88)

    def test_cycle_length_5(self):
        build_cache()
        self.assertEqual(cycle_length(56006), 35)

    def test_cycle_length_6(self):
        build_cache()
        self.assertEqual(cycle_length(154462), 171)

    def test_cycle_length_7(self):
        build_cache()
        self.assertEqual(cycle_length(33886), 60)

    def test_cycle_length_8(self):
        build_cache()
        self.assertEqual(cycle_length(297120), 40)

    def test_cycle_length_9(self):
        build_cache()
        self.assertEqual(cycle_length(742996), 150)

    def test_cycle_length_10(self):
        build_cache()
        self.assertEqual(cycle_length(92541), 134)

    def test_cycle_length_11(self):
        build_cache()
        self.assertEqual(cycle_length(371830), 180)

    def test_cycle_length_12(self):
        build_cache()
        self.assertEqual(cycle_length(6047), 94)

    def test_cycle_length_13(self):
        build_cache()
        self.assertEqual(cycle_length(37565), 63)


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
